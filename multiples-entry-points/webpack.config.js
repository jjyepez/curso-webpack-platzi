//plugin-extract-text

//los modulos se importan as[i]
const path = require('path')

//los plugins se importan como un modulo de node
const ExtractTextPlugin = require('extract-text-webpack-plugin')


module.exports = {
	entry: {
		//entry puede ser solo uno sin {} o varios con nombre en formato json
		home: path.resolve(__dirname,'src/js/index.js'),
		precios: path.resolve(__dirname,'src/js/precios.js'),
		contacto: path.resolve(__dirname,'src/js/contacto.js'),
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/[name].js'
	},
	module: {
		rules: [
			// Aqui van los loaders
			{
				// test: que tipo de archivo quiero reconocer
				// use: que loader se va a encargar del archivo
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					//['style-loader','css-loader']
					fallback: "style-loader",
					use: "css-loader"
				})
			}
		]
	},
	plugins: [
		//aqui van los plugins
		new ExtractTextPlugin('css/[name].css')

	]
}