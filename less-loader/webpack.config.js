//less-style-loader

const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
	entry: path.resolve(__dirname,'src/js/index.js'),
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: './dist/'
	},
	module: {
		rules: [
			// Aqui van los loaders
			{
				// test: que tipo de archivo quiero reconocer
				// use: que loader se va a encargar del archivo
				test: /\.css$/,
				use: ['style-loader','css-loader'],
			},
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015', 'react']
					}
				}
			},
			{
				test: /\.less$/,
				use: ExtractTextPlugin.extract({
					//['style-loader','css-loader']
					fallback: "style-loader",
					use: ['css-loader','less-loader']
				})
			},
			{
				test: /\.json$/,
				use: {
					loader: 'json-loader'
				}
			},
			{
				test: /\.(jpg|png|gif|ttf|svg|otf|eot|woff)$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 100000 // en bytes = 100 Kb
					}
				}
			}
		]
	},
	plugins: [
		new ExtractTextPlugin("./css/teachers.css"),
	]
}
