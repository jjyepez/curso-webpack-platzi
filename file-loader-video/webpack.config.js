//css-style-loader

const path = require('path')

module.exports = {
	entry: path.resolve(__dirname,'src/js/index.js'),
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: './dist/'
	},
	module: {
		rules: [
			// Aqui van los loaders
			{
				// test: que tipo de archivo quiero reconocer
				// use: que loader se va a encargar del archivo
				test: /\.css$/,
				use: ['style-loader','css-loader'],
			},
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015']
					}
				}
			},
			{
				test: /\.(jpg|png|gif|ttf|svg|otf|eot|woff)$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 100000 // en bytes = 100 Kb
					}
				}
			},
			{
				test: /\.(mp4|webm)$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 1000000, // en bytes = 1000 Kb (para base 64)
						name: 'videos/[name].[hash].[ext]' // .. aqui se exportaran cuando superen el limit
					}
				}
			}
		]
	}
}
