import '../css/estilos.css'
import '../css/estilos2.css'
import { firstMessage, delayedMessage } from '../js/message.js'
import platziImg from '../img/platzi.jpg'
import videoPlatzi from '../vid/vid.mp4'

delayedMessage()
document.write( firstMessage )

const img = document.createElement('img')
img.setAttribute('src',platziImg)
document.body.append(img)

const video = document.createElement('video')
video.setAttribute('src', videoPlatzi)
video.setAttribute('controls', true)
video.setAttribute('width','480px')
video.setAttribute('height','320px')
video.setAttribute('autoplay',true)
document.body.append(video)