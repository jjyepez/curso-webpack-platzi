import '../css/estilos.css'
import '../css/estilos2.css'
import { firstMessage, delayedMessage } from '../js/message.js'
import platziImg from '../img/platzi.jpg'
import data from './teachers.json'
import renderToDOM from './renderToDOM.js'
import '../css/teachers.css'

// ------------------ REACT js
import React from 'react'
import { render } from 'react-dom'

import Teachers from './componentes/teachers.js'

render ( <Teachers data={data}/>, document.getElementById('container') )

// -------------------------------------

console.log( data )
data.teachers.forEach( teacher => {
	const elemento = document.createElement('li')
	elemento.textContent = teacher.nombre
	renderToDOM( elemento )
})

delayedMessage()
document.write( firstMessage )

const img = document.createElement('img')
img.setAttribute('src',platziImg)
document.body.append(img)