import renderToDOM from '../js/renderToDOM.js'
import makeMessage from '../js/makeMessage.js'

const waitTime = new Promise( (fncOK, fncERR) => {
	setTimeout( () => {
		fncOK(' Han pasado 3 segundos')
	}, 3000)
})

module.exports = {
	firstMessage: 'Este mensaje viene desde un Modulo!',
	delayedMessage: async () => {
		const message = await waitTime
		console.log( message )
		
		// const element = document.createElement('p')
		// element.textContent = message
		const element = makeMessage( message )
		renderToDOM( element )
	}
}