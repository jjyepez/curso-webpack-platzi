// -- componente Teacher ... creado con el enfoque de modulos React

import React from 'react'

function Teacher( props ) {
	return (
		<li className = "Teacher">
			{props.nombre} <a href={`https://twitter.com/${props.twitter}`}> {props.twitter} </a>
		</li>
	)
}

export default Teacher