import '../css/estilos.css'
import '../css/estilos2.css'
import { firstMessage, delayedMessage } from '../js/message.js'
import platziImg from '../img/platzi.jpg'

import data from './teachers.json'
import renderToDOM from './renderToDOM.js'

console.log( data )
data.teachers.forEach( teacher => {
	const elemento = document.createElement('li')
	elemento.textContent = teacher.nombre
	renderToDOM( elemento )
})

delayedMessage()
document.write( firstMessage )

const img = document.createElement('img')
img.setAttribute('src',platziImg)
document.body.append(img)

fetch(`https://api.coinmarketcap.com/v1/ticker/?limit=10`)
.then( rsp =>  rsp.json() )
.then( rs => {
	console.log( rs )
})